<?php
namespace app\commands;

use Yii;
use yii\console\Controller;
use yii\helpers\Json;

class ImportController extends Controller
{
    /**
     * @param string $filename
     * @throws \yii\db\Exception
     */
    public function actionUsers($filename = 'users.json')
    {
        $rawUsers = $this->decodeJsonFile($filename);
        $users = [];
        foreach ($rawUsers as $user) {
            $users[] = [
                'id' => $user['id'],
                'first_name' => $user['first_name'],
                'last_name' => $user['last_name'],
                'email' => $user['email'],
                'personal_code' => $user['personal_code'],
                'phone' => $user['phone'],
                'active' => $user['active'],
                'dead' => $user['dead'],
                'lang' => $user['lang'],
            ];
        }
        $this->batchInsert('user', array_keys($users[0]), $users);
    }

    /**
     * @param string $filename
     * @throws \yii\db\Exception
     */
    public function actionLoans($filename = 'loans.json')
    {
        $rawLoans = $this->decodeJsonFile($filename);
        $loans = [];
        foreach ($rawLoans as $loan) {
            $loans[] = [
                'id' => $loan['id'],
                'user_id' => $loan['user_id'],
                'amount' => $loan['amount'],
                'interest' => $loan['interest'],
                'duration' => $loan['duration'],
                'start_date' => date(DATE_ISO8601 , $loan['start_date']),
                'end_date' => date(DATE_ISO8601 , $loan['end_date']),
                'campaign' => $loan['campaign'],
                'status' => $loan['status'],
            ];
        }
        $this->batchInsert('loan', array_keys($loans[0]), $loans);
    }

    /**
     * @param $filename
     * @return mixed
     * @throws \Exception
     */
    private function decodeJsonFile($filename)
    {
        try {
            $jsonFile = file_get_contents(Yii::getAlias('@app')  . '/' . $filename);
            $result = Json::decode($jsonFile);
        } catch (\Exception $exception) {
            throw $exception;
        }
        return $result;
    }

    /**
     * @param $table
     * @param $columns
     * @param $data
     * @throws \yii\db\Exception
     */
    private function batchInsert($table, $columns, $data)
    {
        try {
            $connection = Yii::$app->db->createCommand();
            $command = $connection->batchInsert($table, $columns, $data);
            $command->execute();
        } catch (\yii\db\Exception $exception) {
            throw $exception;
        }
    }
}
