<?php

use yii\db\Migration;
use yii\db\Schema;

/**
 * Class m190610_073416_change_loan_status_column_to_integer
 */
class m190610_073416_change_loan_status_column_to_integer extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->alterColumn('loan', 'status', Schema::TYPE_INTEGER . ' NOT NULL USING status::integer');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->alterColumn('loan', 'status', Schema::TYPE_BOOLEAN);
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190610_073416_change_loan_status_column_to_integer cannot be reverted.\n";

        return false;
    }
    */
}
